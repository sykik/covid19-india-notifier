## COVID-19 Notifier

### Usage

    Right now it works for INDIA & INDIAN STATES only.

    Install all the dependancies from `requirements.txt`

    Make changes as per your state, notification timer, message duration or nation to `off`

    if you want status only for the states(default is Uttar Pradesh and Karnataka with timer 1 hour & duration of 5 seconds) in `config.ini` file.

    While adding or  removing list of states in `config.ini` make sure the values are comma('`,`') seperated.

    Then execute the `notefy.py` and it will notify you as per time set by you.

### Available States

    States =  Andaman and Nicobar Islands,
                    Andhra Pradesh,
                    Arunachal Pradesh,
                    Assam,
                    Bihar,
                    Chandigarh,
                    Chhattisgarh,
                    Dadra and Nagar Haveli,
                    Daman and Diu,
                    Delhi,
                    Goa,
                    Gujarat,
                    Haryana,
                    Himachal Pradesh,
                    Jammu and Kashmir,
                    Jharkhand,
                    Karnataka,
                    Kerala,
                    Ladakh,
                    Lakshadweep,
                    Madhya Pradesh,
                    Maharashtra,
                    Manipur,
                    Meghalaya,
                    Mizoram,
                    Nagaland,
                    Odisha,
                    Puducherry,
                    Punjab,
                    Rajasthan,
                    Sikkim,
                    Tamil Nadu,
                    Telangana,
                    Tripura,
                    Uttar Pradesh,
                    Uttarakhand,
                    West Bengal

### Directory Structure

                    COVID-19 Notifier
                    ├── config.ini
                    ├── notify.py
                    └── README.md